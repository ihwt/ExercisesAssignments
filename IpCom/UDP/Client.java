package com.Projects.IpCom.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		DatagramSocket cds = new DatagramSocket();
		// 字节数组
		
		while (true) {
			System.out.print("请输入要发送的数据（exit 退出）：");
			String next = sc.next();
			if (next.equals("exit")) {
				System.out.println("退出成功！");
				cds.close();
				break;
			}
			byte[] bytes = next.getBytes();
			DatagramPacket dp = new DatagramPacket(bytes, bytes.length, InetAddress.getLocalHost(), 8888);
			cds.send(dp);
			System.out.println("发送成功！");
		}
		
	}
}
