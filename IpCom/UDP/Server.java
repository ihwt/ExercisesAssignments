package com.Projects.IpCom.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Server {
	public static void main(String[] args) throws Exception {
		DatagramSocket sds = new DatagramSocket(8888);
		
		while (true) {
			byte[] bytes = new byte[1024 * 64];
			DatagramPacket sdp = new DatagramPacket(bytes, bytes.length);
			sds.receive(sdp);
			String s = new String(bytes, 0, sdp.getLength());
			System.out.println(s);
			System.out.println(sdp.getAddress().getHostAddress());
			System.out.println(sdp.getPort());

//			sds.close();
		}
	}
}
