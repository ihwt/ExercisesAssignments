package com.Projects.IpCom.TCP;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
	public static List<Socket> onLineSocket = new ArrayList<>();
	
	public static void main(String[] args) throws Exception {
		System.out.println("\t===服务器启动成功===");
		ServerSocket ss = new ServerSocket(8888);
//		// 创建连接管道
//		Socket acc = ss.accept();
//		// 字节流入和流出必须相同
//		DataInputStream dis = new DataInputStream(acc.getInputStream());
////		String client = acc.getRemoteSocketAddress().toString().split("/")[1];
//		String client = acc.getRemoteSocketAddress().toString().replace("/", "");
//		while (true) {
//			try {
//				// 字节输入utf 获取必须相同
//				String r = dis.readUTF();
//				System.out.println(client + " 发送：" + r);
////			System.out.println(acc.getLocalAddress());
////			System.out.println(acc.getPort());
//			} catch (IOException e) {
//				System.out.println(client + " 连接失败！");
//				dis.close();
//				ss.close();
//				break;
//			}
//		}
//		is.close();
//		ss.close();
		
		// 多客户端发送消息接收
		while (true) {
			// 创建连接管道
			Socket acc = ss.accept();
			onLineSocket.add(acc);
			String client = acc.getRemoteSocketAddress().toString().replace("/", "");
			System.out.println(" 客户端 " + client + " 已上线！");
			new ServerThread(acc).start();
		}
	}
}
