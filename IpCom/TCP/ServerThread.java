package com.Projects.IpCom.TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;


public class ServerThread extends Thread {
	private final Socket acc;
	
	public ServerThread(Socket acc) {
		this.acc = acc;
	}
	
	public static void SendMsg(String msg) throws Exception {
		for (Socket socket : Server.onLineSocket) {
			DataOutputStream dis = new DataOutputStream(socket.getOutputStream());
			dis.writeUTF(msg);
			dis.flush();
		}
	}
	
	@Override
	public void run() {
		try {
			DataInputStream dis = new DataInputStream(acc.getInputStream());
			String client = acc.getRemoteSocketAddress().toString().replace("/", "");
			while (true) {
				try {
					// 字节输入utf 获取必须相同
					String r = dis.readUTF();
					System.out.println(" 客户端 " + client + " 发送：" + r);
					SendMsg(r);
				} catch (Exception e) {
					System.out.println(" 客户端" + client + "已下线！");
					Server.onLineSocket.remove(acc);
					dis.close();
					acc.close();
					break;
				}
			}
		} catch (IOException e) {
			System.out.println(" 客户端连接失败！");
		}
	}
}
