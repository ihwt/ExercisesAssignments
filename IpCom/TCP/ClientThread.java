package com.Projects.IpCom.TCP;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientThread extends Thread {
	private final Socket acc;
	
	public ClientThread(Socket acc) {
		this.acc = acc;
	}
	
	@Override
	public void run() {
		try {
			DataInputStream dis = new DataInputStream(acc.getInputStream());
			String client = acc.getRemoteSocketAddress().toString().replace("/", "");
			while (true) {
				try {
					// 字节输入utf 获取必须相同
					String r = dis.readUTF();
					System.out.println(" 客户端 " + client + " 发送：" + r);
				} catch (Exception e) {
					System.out.println(" 客户端" + client + "已下线！");
					dis.close();
					acc.close();
					break;
				}
			}
		} catch (IOException e) {
			System.out.println(" 服务端连接失败！");
		}
	}
}
