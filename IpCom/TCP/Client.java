package com.Projects.IpCom.TCP;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) throws Exception {
		System.out.println("\t===客户端启动成功===");
		Scanner sc = new Scanner(System.in);
		// 创建服务端连接
		Socket st = new Socket("127.0.0.1", 8888);
		new ClientThread(st).start();
		
		// 创建字节输出流
		OutputStream os = st.getOutputStream();
		DataOutputStream dos = new DataOutputStream(os);
		while (true) {
			System.out.print("请输入要发送的数据（exit 退出）：");
			String next = sc.next();
			if (next.equals("exit")) {
				System.out.println(" 退出成功！");
				dos.close();
				st.close();
				break;
			}
			try {
				dos.writeUTF(next);
				// 立即发送到服务端 防止数据在内存中
				dos.flush();
				System.out.println(" 发送成功！");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println(" 服务器连接失败！");
				dos.close();
				st.close();
				break;
			}
		}
		// 关闭连接
//		dos.close();
//		st.close();
	}
}
