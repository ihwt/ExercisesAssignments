package com.Projects.AtmPay;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ATM {
	private final ArrayList<Account> accounts = new ArrayList<>();
	Scanner sc = new Scanner(System.in);
	Random r = new Random();
	
	// 启动页面
	public void start() {
		OUT:
		while (true) {
			System.out.println("\n\t=====欢迎进入ATM系统=====\n");
			System.out.println(" 0、退出系统\t1、用户登录\t2、业务开户");
			System.out.print("请选择要办理的业务:");
			int business = sc.nextInt();
			switch (business) {
				case 0:
					System.out.println("退出成功！！！");
					break OUT;
				case 1:
					System.out.println("\n\t========用户登录========\n");
					login();
					break;
				case 2:
					System.out.println("\n\t========业务开户========\n");
					while (true) {
						System.out.println(" 输入0：返回上一级菜单\t输入1：继续办理此业务");
						System.out.print("请输入指令：");
						int num = sc.nextInt();
						switch (num) {
							case 0:
								System.out.println("已返回上一级菜单！！！\n");
								start();
								break OUT;
							case 1:
								createAccount();
								break;
							default:
								System.out.println("输入错误，系统将重新执行此命令！！！");
						}
					}
				default:
					System.out.println("\n操作错误，即将重新进入系统...");
			}
		}
	}
	
	// 登录
	private void login() {
		if (accounts.isEmpty()) {
			System.out.println("系统不存在任何账户，将自动跳转开户业务...\n");
			createAccount();
			return;
		}
		System.out.print("请输入您的账户：");
		Account account = getAccount(sc.next());
		if (account == null) {
			System.out.print("系统中不存在该账户，");
			login();
			return;
		}
		String psw = account.getPassword();
		OUT:
		while (true) {
			System.out.print("请输入该账户密码：");
			String password = sc.next();
			if (psw.equals(password)) {
				System.out.println("恭喜 " + account.getUsername() + " 登录成功");
				while (true) {
					System.out.println("指令：0 回到欢迎页\t指令：123 验证本人并继续操作");
					System.out.print("请输入指令：");
					int state = sc.nextInt();
					switch (state) {
						case 0:
							System.out.println("\n欢迎回来！");
							break OUT;
						case 123:
							System.out.print("验证成功，");
							operation(account);
							break OUT;
						default:
							System.out.print("输入错误，请重新输入：");
					}
				}
			}
			System.out.print("密码输入错误，");
		}
	}
	
	// 登录成功后操作
	private void operation(Account at) {
		OUT:
		while (true) {
			System.out.print("继续执行以下业务：\n");
			System.out.println(" 1、账户信息\t2、账户存款\t3、账户取款\n 4、账户转账\t5、修改密码\t6、退出账户\n 7、注销账户");
			System.out.print("请选择指令：");
			int num = sc.nextInt();
			switch (num) {
				case 1:
					System.out.println("\t=======当前账户信息=======\n" + "\t卡号：" + at.getCardId() + ";\n" + "\t姓名：" + at.getUsername() + ";\n" +
						"\t性别：" + at.getSex() + ";\n" + "\t余额：" + at.getBalance() + ";\n" + "\t限额：" + at.getLimit() +
						";");
					break;
				case 2:
					while (true) {
						System.out.print("请输入存款金额：");
						double deposit = sc.nextDouble();
						if (deposit > 0) {
							at.setBalance(at.getBalance() + deposit);
							System.out.println("存款成功！当前帐户余额：" + at.getBalance() + "元！");
							break;
						}
						System.out.print("存款失败,存款金额应大于0元,");
					}
					break;
				case 3:
					while (true) {
						System.out.print("请输入取款金额：");
						double balance = sc.nextDouble();
						if (at.getBalance() > 0) {
							if (balance <= at.getLimit() && balance > 0) {
								at.setBalance(at.getBalance() - balance);
								System.out.println("取款成功！当前帐户余额：" + at.getBalance() + "元！");
								break;
							}
						}
						System.out.print("取款失败,单次取款应在 1~" + (int) at.getLimit() + " 之间,");
					}
					break;
				case 4:
					OUTER:
					while (true) {
						if (accounts.size() > 1) {
							while (true) {
								System.out.print("请输入转账对象卡号：");
								String id = sc.next();
								Account otherAcc = getAccount(id);
								if (otherAcc != null) {
									if (at.getBalance() > 0) {
										while (true) {
											System.out.print("请输入转账金额：");
											double transfer = sc.nextDouble();
											if (transfer <= at.getLimit() && transfer > 0) {
												at.setBalance(at.getBalance() - transfer);
												otherAcc.setBalance(otherAcc.getBalance() + transfer);
												System.out.println("转账成功，当前帐户余额：" + at.getBalance() + "元！");
												break OUTER;
											}
											System.out.print("转账失败,单次转账应在 1~" + (int) at.getLimit() + " 之间,");
										}
									}
									System.out.println("当前帐户余额为：" + at.getBalance() + "请先去存款!");
									break;
								}
								System.out.print("转账对象不存在,");
								break;
							}
						}
						System.out.println("当前系统用户唯一，将自动跳转开户业务...");
						createAccount();
						break;
					}
					break;
				case 5:
					System.out.print("请输入当前账户密码：");
					while (true) {
						String psw = sc.next();
						if (at.getPassword().equals(psw)) {
							System.out.print("请设置账户新密码：");
							while (true) {
								String newPassword = sc.next();
								if (newPassword.length() < 6) {
									System.out.print("密码长度不能小于6位，请重新输入：");
									continue;
								}
								System.out.print("请确认账户新密码：");
								String okPassword = sc.next();
								if (okPassword.equals(newPassword)) {
									if (!okPassword.equals(at.getPassword())) {
										at.setPassword(okPassword);
										System.out.println("修改密码成功！！！");
										break OUT;
									}
									System.out.print("新密码不能与旧密码相同，请重新设置：");
									continue;
								}
								System.out.print("两次密码不一致，请重新设置：");
							}
						}
						System.out.print("当前帐户密码输入错误，将重新输入：");
					}
				case 6:
					System.out.println(at.getCardId() + " 账户退出成功！！！");
					break OUT;
				case 7:
					accounts.remove(at);
					System.out.println("账户删除成功！！！");
//					login();
					break OUT;
				default:
					System.out.print("指令输入错误，");
			}
		}
	}
	
	
	//	开户
	private void createAccount() {
		Account acc = new Account();
		System.out.println("请输入开户信息：");
		// 姓名
		System.out.print("姓名：");
		acc.setUsername(sc.next());
		// 性别
		while (true) {
			System.out.print("性别（男/1 or 女/0）：");
			String sex = sc.next();
			if (sex.equals("男") || sex.equals("女") || sex.equals("0") || sex.equals("1")) {
				acc.setSex(sex);
				break;
			}
			System.out.print("性别输入错误，请重新输入");
		}

//	设置密码
		while (true) {
			System.out.print("设置密码(最低6位)：");
			String psw = sc.next();
			if (psw.length() < 6) {
				System.out.print("密码长度小于6位，请重新");
				continue;
			}
			System.out.print("请确认您输入的密码：");
			String password = sc.next();
			if (password.equals(psw)) {
				acc.setPassword(password);
				break;
			}
			System.out.print("两次密码不一致，请重新");
		}
		while (true) {
			System.out.print("请设置账户使用限制额度：");
			double limit = sc.nextDouble();
			if (limit >= 1000 && limit <= 300000) {
				acc.setLimit(limit);
				acc.setBalance(limit);
				System.out.println("设置成功！默认自动为当前账户存款" + limit + "元");
				break;
			}
			System.out.print("账户限制额度应在1000 ~ 300000元之间，");
		}
		String accountId = setAccountId();
		acc.setCardId(accountId);
		accounts.add(acc);
		setSysClipboardText(accountId);
		System.out.println("\n恭喜您\t" + acc.getUsername() + " 开户成功，您的账户是：" + accountId + "\t/* 系统已自动将账户号复制在剪贴板,按下 " +
			"ctrl+v " +
			"即可直接使用 */");
	}
	
	//	设置不重复卡号
	private String setAccountId() {
		StringBuilder id = new StringBuilder("1");
		for (int i = 0; i < 10; i++) {
			id.append(r.nextInt(10));
		}
		Account currentAccount = getAccount(id.toString());
		if (currentAccount != null) {
			setAccountId();
		}
		return id.toString();
	}
	
	// 查找卡号
	private Account getAccount(String id) {
		for (Account account : accounts) {
			if (account.getCardId().equals(id)) {
				return account;
			}
		}
		return null;
	}
	
	
	// 将传入的字符串内容复制到剪切板
	public void setSysClipboardText(String writeMe) {
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable tText = new StringSelection(writeMe);
		clip.setContents(tText, null);
	}
}
