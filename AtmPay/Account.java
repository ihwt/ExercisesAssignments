package com.Projects.AtmPay;

public class Account {
	private String cardId; // 卡号
	private String username; // 姓名
	private String sex; // 性别
	private String password; // 密码
	private double balance; // 余额
	private double limit; // 限额
	
	public Account() {
	}
	
	public Account(String cardId, String username, String sex, String password, double balance, double limit) {
		this.cardId = cardId;
		this.username = username;
		this.sex = sex;
		this.password = password;
		this.balance = balance;
		this.limit = limit;
	}
	
	public String getCardId() {
		return cardId;
	}
	
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
	public String getUsername() {
		return username + (sex.equals("男") || sex.equals("1") ? "先生" : "女士");
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getSex() {
		if (sex.equals("男") || sex.equals("1")) {
			return "男";
		} else {
			return "女";
		}
	}
	
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public double getLimit() {
		return limit;
	}
	
	public void setLimit(double limit) {
		this.limit = limit;
	}
}
