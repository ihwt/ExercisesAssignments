package com.Projects.StoneGame;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class MainFrame extends JFrame implements KeyListener {
	Random r = new Random();
	int row = 0;
	int col = 0;
	int step = 0;
	int[][] data = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 0}};
	int[][] datas = new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 0}};
	
	//	JFrame frame = new JFrame();
	public MainFrame() {
		stoneG();
	}
	
	private void stoneG() {
		setSize(514, 595); // 设置大小
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // 设置关闭模式
		setTitle("石头游戏单机版--StoneGame--V1.0"); // 设置标题
		setLocationRelativeTo(null); // 设置布局
		setAlwaysOnTop(true); // 置顶
		setLayout(null); // 取消默认布局
		disrupt(); // 打乱顺序
		generate(); // 创建方块
		this.addKeyListener(this); //注册监听时间
		
		setVisible(true); // 显示
	}
	
	
	private void generate() {
		getContentPane().removeAll();
		if (victory()) {
			JLabel jl = new JLabel(new ImageIcon("Javaproject\\src\\com\\Games\\StoneGame\\image\\win.png"
			));
			jl.setBounds(124, 230, 266, 88);
			getContentPane().add(jl);
		}
		JLabel steps = new JLabel("步数：" + step);
		steps.setBounds(50, 18, 100, 20);
		getContentPane().add(steps);
		JButton btn = new JButton("新游戏");
		btn.setBounds(350, 18, 100, 20);
		getContentPane().add(btn);
		btn.setFocusable(false);
		btn.addActionListener(e -> {
			step = 0;
			disrupt();
			generate();
		});
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				JLabel jl =
					new JLabel(new ImageIcon("Javaproject\\src\\com\\Games\\StoneGame\\image\\" + data[i][j] + ".png"
				));
				jl.setBounds(50 + j * 100, 90 + 100 * i, 100, 100);
				getContentPane().add(jl);
			}
		}
		JLabel jl = new JLabel(new ImageIcon("Javaproject\\src\\com\\Games\\StoneGame\\image\\background.png"
		));
		jl.setBounds(26, 30, 450, 484);
		getContentPane().add(jl);
		getContentPane().repaint();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
//		System.out.println(e.getKeyCode());
		move(e.getKeyCode());
		generate();
	}
	
	// 打乱顺序
	private void disrupt() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				int x = r.nextInt(data.length);
				int y = r.nextInt(data[i].length);
				int temp = data[i][j];
				data[i][j] = data[x][y];
				data[x][y] = temp;
			}
		}
		
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				if (data[i][j] == 0) {
					row = i;
					col = j;
				}
			}
		}
	}
	
	
	private void move(int keyCode) {
		if (victory()) {
			return;
		}
		if (keyCode == 37 || keyCode == 65) {
			if (col == 3) {
				return;
			}
			int temp = data[row][col];
			data[row][col] = data[row][col + 1];
			data[row][col + 1] = temp;
			col++;
		} else if (keyCode == 38 || keyCode == 87) {
			if (row == 3) {
				return;
			}
			int temp = data[row][col];
			data[row][col] = data[row + 1][col];
			data[row + 1][col] = temp;
			row++;
		} else if (keyCode == 39 || keyCode == 68) {
			if (col == 0) {
				return;
			}
			int temp = data[row][col];
			data[row][col] = data[row][col - 1];
			data[row][col - 1] = temp;
			col--;
		} else if (keyCode == 40 || keyCode == 83) {
			if (row == 0) {
				return;
			}
			int temp = data[row][col];
			data[row][col] = data[row - 1][col];
			data[row - 1][col] = temp;
			row--;
		} else if (keyCode == 82) {
			data = new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 0}};
			row = 3;
			col = 3;
		} else {
			step--;
			System.out.println("Invalid key");
		}
		step++;
	}
	
	// 判断是否胜利
	private boolean victory() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < datas[i].length; j++) {
				if (data[i][j] != datas[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
	}
}
