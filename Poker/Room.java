package com.Projects.Poker;

import java.util.*;

public class Room {
	private final List<Card> allCards = new ArrayList<>();
	Scanner sc = new Scanner(System.in);
	
	// 创建新牌
	private void createCard() {
		String[] name = {"3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2"};
		String[] color = {"♥", "♣", "♠", "♦"};
		int w = 0;
		for (String s : name) {
			w++;
			for (String c : color) {
				allCards.add(new Card(s, c, w));
			}
		}
		Collections.addAll(allCards, new Card("小王", "k", w + 1), new Card("大王", "K", w + 2));
	}
	
	public void shuffleCard() {
		createCard();
		// 打乱牌顺序
		Collections.shuffle(allCards);
		// 创建玩家
		ArrayList<Card> player1 = new ArrayList<>();
		ArrayList<Card> player2 = new ArrayList<>();
		ArrayList<Card> player3 = new ArrayList<>();
		// 发牌
		for (int i = 0; i < allCards.size() - 3; i++) {
			if (i % 3 == 0) {
				player1.add(allCards.get(i));
			} else if (i % 3 == 1) {
				player2.add(allCards.get(i));
			} else {
				player3.add(allCards.get(i));
			}
		}
		sortCard(player1);
		sortCard(player2);
		sortCard(player3);
		System.out.println("玩家1：" + player1);
		System.out.println("玩家2：" + player2);
		System.out.println("玩家3：" + player3);
		// 地主牌
		List<Card> landlord = allCards.subList(allCards.size() - 3, allCards.size());
		for (int i = 1; i <= 3; i++) {
			System.out.print("玩家" + i + "开始叫牌(3分为抢到地主)：");
			int j = sc.nextInt();
			if (j > 3 || j < 0) {
				System.out.println("输入不合法，将重新开始：");
				shuffleCard();
				return;
			}
			if (j == 3) {
				System.out.println("玩家" + i + "抢到地主！");
				if (i == 1) {
					player1.addAll(landlord);
					sortCard(player1);
					System.out.println("玩家1：" + player1);
				} else if (i == 2) {
					player2.addAll(landlord);
					sortCard(player2);
					System.out.println("玩家2：" + player2);
				} else {
					player3.addAll(landlord);
					sortCard(player3);
					System.out.println("玩家3：" + player3);
				}
				break;
			}
		}
	}
	
	// 重新排序
	private void sortCard(List<Card> card) {
		card.sort(Comparator.comparingInt(Card::getWeight));
	}
}
