package com.Projects.Exam.demo22;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
	
	/*
		某个班级组织团建活动，班长给出了几个去处给大家选择，分别是 “农家乐” , "轰趴"，“野外拓展”，“健身房”，本次活动每个学生是可以多选的。现在有如下5名学生选择了如下去处。
				张全蛋儿  农家乐,野外拓展
				李二狗子  轰趴,野外拓展,健身房
				翠花     野外拓展
				小帅     轰趴，健身房
				有容     农家乐
		具体的功能点如下：
			请找出每个去处想去的人数是多少，并输出投票最多的去处是哪个
			请找出哪些人没有选择投票最多的去处，输出他们的名字
				比如：小帅，有容没有选择野外拓展。
		*/
	public static void main(String[] args) {
		Map<String, String[]> sa = new HashMap<>();
		
		sa.put("张全蛋儿", new String[]{"农家乐", "野外拓展"});
		sa.put("李二狗子", new String[]{"轰趴", "野外拓展",});
		sa.put("翠花", new String[]{"野外拓展"});
		sa.put("小帅", new String[]{"轰趴", "健身房"});
		sa.put("有容", new String[]{"农家乐"});

//		System.out.println(sa);
		Map<String, Integer> sum = new HashMap<>();
		for (String key : sa.keySet()) {
			String[] value = sa.get(key);
			for (String s : value) {
				if (sum.containsKey(s)) {
					sum.put(s, sum.get(s) + 1);
					continue;
				}
				sum.put(s, 1);
			}
		}
		
		String max = "轰趴";
		for (String key : sum.keySet()) {
			if (sum.get(key) > sum.get(max)) {
				max = key;
			}
			System.out.print(key + "-->投票人数：" + sum.get(key) + ";\t");
		}
		System.out.println("\n 人数最多的是：" + max);
		System.out.println(sum);
		List<String> iname = new ArrayList<>();
		for (Map.Entry<String, String[]> entries : sa.entrySet()) {
			boolean flag = false;
			for (String s : entries.getValue()) {
				if (s.equals(max)) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				iname.add(entries.getKey());
				System.out.println("没有选择" + max + "的有" + entries.getKey());
			}
		}
		System.out.println("没有选择" + max + "的有" + iname);
	}
}
