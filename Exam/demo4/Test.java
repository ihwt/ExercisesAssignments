package com.Projects.Exam.demo4;

public class Test {
	public static void main(String[] args) {
		MyArrayList<String> list = new MyArrayList<>();
		list.add("123");
		list.add("222");
		list.add("321");
		System.out.println(list.toString());
		
		System.out.println(list.size());
		System.out.println(list.get(2));
		System.out.println(list.remove(1));
		System.out.println(list.toString());
		
		list.forEach(System.out::println);
	}
}
