package com.Projects.Exam.demo33;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {

/*
	黑马程序员教学管理系统的菜单信息如下，菜单的详细数据存储在给的素材文件“菜单.txt”中。具体要实现的功能点如下所示:
		请从系统菜单.txt中读取这些菜单信息，将这些菜单信息在控制台展示成图1的样子（必须确保展示的顺序是正确的）
		将正确的菜单顺序，写出到一个新文件**"菜单2.txt"**中保存起来
 */
	
	public static void main(String[] args) {
		
		try (
			BufferedReader br = new BufferedReader(new FileReader("Javaproject\\src\\com\\project\\Exam\\demo33\\菜单" +
				".txt"));
			PrintStream fw = new PrintStream("Javaproject\\src\\com\\project\\Exam\\demo33\\菜单2.txt");
		) {
			List<String> l = new ArrayList<>();
			String data = null;
			while ((data = br.readLine()) != null) {
				l.add(data);
			}
			Collections.sort(l);
			for (String s : l) {
				String[] st = s.split("-");
				System.out.println(st[0].length() == 4 ? st[1] : "\t" + st[1]);
				if (st[0].length() == 4) {
					fw.println("(" + st[0] + ") " + st[1]);
				} else {
					fw.println("\t" + "(" + st[0] + ") " + st[1]);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
