package com.Projects.Exam.demo55;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	private final Scanner sc = new Scanner(System.in);
	private Socket socket;
	
	public void start() throws Exception {
		socket = new Socket("127.0.0.1", 8888);
		System.out.println("------服务端连接成功------");
		// 1、开发界面。
		// 2、提示功能
		OUT:
		while (true) {
			System.out.print("请在以下命令中选择：1、注册 2、登录 3、退出\n");
			System.out.print("请选择：");
			String command = sc.next();
			switch (command) {
				case "1":
					// 注册
					register();
					break;
				case "2":
					// 登录
					login();
					break;
				case "3":
					System.out.println("退出系统！");
					socket.close();
					break OUT;
			}
		}
	}
	
	private void register() throws Exception {
		System.out.println("==============注册==================");
		logRes(1);
	}
	
	private void login() throws Exception {
		System.out.println("==============登录==================");
		logRes(2);
	}
	
	private void logRes(int a) throws Exception {
		System.out.println("开始输入" + (a == 1 ? "注册" : "登录") + "信息");
		System.out.print("账户名：");
		String name = sc.next();
		
		System.out.print("密码：");
		String passWord = sc.next();
		
		// 2、发送登录名和密码给服务端保存
		DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
		dos.writeInt(a); // 登录
		dos.writeUTF(name);
		dos.writeUTF(passWord);
		dos.flush();
		
		// 3、马上接收服务端响应。
		DataInputStream dis = new DataInputStream(socket.getInputStream());
		System.out.println(dis.readUTF());
	}
	
}
