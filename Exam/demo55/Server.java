package com.Projects.Exam.demo55;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static final String USERS_FILE_PATH = "Javaproject\\src\\com\\project\\Exam\\demo55\\users.properties";
	
	public static void main(String[] args) {
		System.out.println("------服务端启动成功------");
		try (ServerSocket ss = new ServerSocket(8888);) {
			while (true) {
				Socket acc = ss.accept();
				new ServerThread(acc).start();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}


