package com.Projects.Exam.demo3;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {

/*
	某护士小花，作息规律为上二天班，休息一天，经常不确定休息日是否是周末（注：首次休息日是2022年2月3日）。	具体功能点的要求如下:
		1、请你开发一个程序，当小花输入年以及月后，立即显示出该月份的休息日详情。
			示范（注意：示范信息重点在于参考格式，结果不一定是准确的，请自行确保计算结果正确性）：
			请小花输入查询的月份（月份必须是2022年2月之后的月份）： 2023-5 。2023-5-1[休息] 2023-5-2 2023-5-3 2023-5-4[休息] ...
		2、显示出该月份哪些休息日是周六或周日（请依次列出具体的日期和其星期信息）。
		3、小花给自己设置了一个高考倒计时。高考的开始时间为：2023年06月07日 上午9：00 。请利用给的素材代码（在Timer文件夹下），补全代码，产生一个如下的倒计时效果，
					略。
*/
	
	public static void main(String[] args) {
		LocalDateTime startDate = LocalDateTime.of(2022, 2, 3, 0, 0, 0);
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入想查询的年月（格式：2022-02）：");
		String str = sc.next();
		LocalDateTime endDate = LocalDateTime.parse(str + "-01T00:00:00");
		int nowday = endDate.plusMonths(1).minusDays(1).getDayOfMonth();
		
		// 计算两个时间相差时间
		Duration dt = Duration.between(startDate, endDate);
		List<Day> days = new ArrayList<>();
		for (int i = 0; i < dt.toDays() + nowday; i++) {
			if (i % 3 == 0) {
				days.add(new Day(startDate.plusDays(i), true));
				continue;
			}
			days.add(new Day(startDate.plusDays(i), false));
		}
		
		for (Day day : days) {
			if (day.getDay().toString().contains(str)) {
				int i = day.getDay().getDayOfWeek().getValue();
				if (day.isDay() && (i == 6 || i == 7)) {
					System.out.println("\n" + day + "  星期" + (i == 6 ? "六" : "日"));
				} else {
					System.out.print(day + "\t");
				}
			}
		}
	}
}
