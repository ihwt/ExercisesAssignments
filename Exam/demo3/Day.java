package com.Projects.Exam.demo3;

import java.time.LocalDateTime;

public class Day {
	private LocalDateTime day;
	private boolean isDay;
	
	public Day() {
	}
	
	public Day(LocalDateTime day, boolean isDay) {
		this.day = day;
		this.isDay = isDay;
	}
	
	public LocalDateTime getDay() {
		return day;
	}
	
	public boolean isDay() {
		return isDay;
	}
	
	public void setDay(LocalDateTime day) {
		this.day = day;
	}
	
	public void setDay(boolean day) {
		isDay = day;
	}
	
	@Override
	public String toString() {
		return day.getYear() + "-" + day.getMonthValue() + "-" + day.getDayOfMonth() + " : " + (isDay ? "[休息]" :
			"[工作]");
	}
}
