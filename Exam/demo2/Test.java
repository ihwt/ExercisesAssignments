package com.Projects.Exam.demo2;

import java.time.LocalDate;
import java.util.*;

public class Test {

/*
	业务一：有如下字符串，里面包含多个用户信息数据，现在需要你解析这个字符串，获取里面的用户数据，并封装到User对象中多个User对象在添加到List<User> 集合中;注意：字符串中的规则如下，多个用户用 #
	拼接，用户的信息之间用 : 拼接。其中用户id和生日是需要进行类型转换的，其中id需要将String转成Long，生日需要将String转成LocalDate.
	业务二：遍历上面获取的List<User> 集合，统计里面每个名字出现的次数。封装到Map<String,Integer>集合中，集合的key就是名字，value就是名字出现的次数。最后遍历打印map数据，打印内容如下：张三：3次
	李四：5次
*/
	
	public static void main(String[] args) {
		List<User> users = new ArrayList<>();
		String userStrs = "10001:张三:男:1990-01-01#10002:李四:女:1989-01-09#10003:王五:男:1999-09-09#10004:刘备:男:1899-01-01" +
			"#10005" +
			":孙悟空:男:1900-01-01#10006:张三:女:1999-01-01#10007:刘备:女:1999-01-01#10008:张三:女:2003-07-01#10009:猪八戒:男:1900-01" +
			"-01";
		
		String[] info = userStrs.split("#");
		for (String userStr : info) {
			String[] userInfo = userStr.split(":");
			Collections.addAll(users, new User(Long.valueOf(userInfo[0]), userInfo[1], userInfo[2],
				LocalDate.parse(userInfo[3])));
		}
		System.out.println(users);
		
		
		Map<String, Integer> us = new HashMap<>();
//		for (User user : users) {
//			String name = user.getName();
//			int i = 0;
//			for (User user1 : users) {
//				if (user1.getName().equals(name)) {
//					i++;
//				}
//			}
//			us.put(name, i);
//		}
		for (User user : users) {
			if (us.containsKey(user.getName())) us.put(user.getName(), us.get(user.getName()) + 1);
			us.put(user.getName(), 1);
		}


//		for (String s : us.keySet()) {
//			System.out.println(s + "：" + us.get(s) + "次");
//		}
		us.keySet().forEach(s -> System.out.println(s + "：" + us.get(s) + "次"));
		
	}
}
