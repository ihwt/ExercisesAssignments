package com.Projects.Exam.demo2;

import java.time.LocalDate;

public class User {
	private Long id; // 用户id
	private String name; //性别
	private String gender; //性别
	private LocalDate birthday; //生日
	
	public User() {
	}
	
	public User(Long id, String name, String gender, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.birthday = birthday;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public LocalDate getBirthday() {
		return birthday;
	}
	
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	@Override
	public String toString() {
		return id + ";" + name + ";" + gender + ";" + birthday;
	}
}
