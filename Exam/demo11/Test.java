package com.Projects.Exam.demo11;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Test {

/*
	某小型商城系统的订单信息在素材下的orders.xml文件中，现在要求把xml中的订单信息，封装成一个一个的订单对象，将订单对象保存到ArrayList集合中。具体功能点要求:
			定义订单类Order，创建ArrayList集合，用于存储订单Order对象
			请使用Stream流找出今天之前的订单，并遍历输出
			请使用Stream流找出集合中价格最贵的订流单，把这个订单的详细信息打印出来
			请使用Stream流遍历集合中的每个订单，要求按照价格降序输出每个订单的详情
	*/
	
	public static void main(String[] args) throws Exception {
		List<Order> Orders = new ArrayList<>();
		SAXReader sax = new SAXReader();
		Document document = sax.read(Test.class.getResourceAsStream("orders.xml"));
//		Document document = sax.read("Javaproject\\src\\com\\project\\Exam\\demo11\\orders.xml");
		List<Element> order = document.getRootElement().elements("order");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		for (Element element : order) {
			Order orders = new Order();
			orders.setId(Integer.parseInt(element.attributeValue("id")));
			orders.setName(element.elementText("name"));
			orders.setTime(LocalDateTime.parse(element.elementText("time"), dtf));
			orders.setPrice(Double.parseDouble(element.elementText("double")));
			Orders.add(orders);
		}
		System.out.println(Orders);
		// 今天之前的订单
		System.out.print("今天之前的订单:");
		Orders.stream().filter(v -> v.getTime().isBefore(LocalDateTime.now())).forEach(System.out::print);
		// 价格最贵的订单
		System.out.print("价格最贵的订单:");
		System.out.println(Orders.stream().max(Comparator.comparingDouble(Order::getPrice)).get());
		// 全部订单按照价格降序
		System.out.print("全部订单按照价格降序:");
		Orders.stream().sorted((o1, o2) -> Double.compare(o2.getPrice(), o1.getPrice())).forEach(System.out::print);
	}
}
