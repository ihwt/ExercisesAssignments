package com.Projects.Exam.demo1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomNumber {

/*
	目前有100名囚犯，每个囚犯的编号是1-200之间的随机数。现在要求依次随机生成100名囚犯的编号（要求这些囚犯的编号是不能重复的），然后让他们依次站成一排。(注：位置是从1开始计数的)
	，接下来，国王命令手下先干掉全部奇数位置处的人。剩下的人，又从新按位置1开始，再次干掉全部奇数位置处的人，依此类推，直到最后剩下一个人为止，剩下的这个人为幸存者。
	具体功能点的要求如下：请输出幸存者的编号，以及他第一次所占的位置值是多少。
*/
	
	static List<People> allPeoples = new ArrayList<>();
	
	public static void main(String[] args) {
		Random rand = new Random();
		
		while (allPeoples.size() < 100) {
			int num = rand.nextInt(200) + 1;
			if (isCode(num)) {
				allPeoples.add(new People(num, allPeoples.size() + 1));
			}
		}
		System.out.println(allPeoples);
		while (allPeoples.size() > 1) {
			List<People> last = new ArrayList<>();
			for (int i = 0; i < allPeoples.size(); i++) {
				if ((i + 1) % 2 == 0) {
					last.add(allPeoples.get(i));
				}
			}
			allPeoples = last;
		}
		System.out.println(allPeoples);
	}
	
	private static boolean isCode(int num) {
		for (People p : allPeoples) {
			if (p.getCode() == num) {
				return false;
			}
		}
		return true;
	}
}
