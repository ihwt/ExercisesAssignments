package com.Projects.Exam.demo1;

public class People {
	private int code;
	private int local;
	
	public People() {
	}
	
	public People(int code, int local) {
		this.code = code;
		this.local = local;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public int getLocal() {
		return local;
	}
	
	public void setLocal(int local) {
		this.local = local;
	}
	
	@Override
	public String toString() {
		return code + "-->" + local;
	}
}
