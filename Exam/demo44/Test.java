package com.Projects.Exam.demo44;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Test {

/*
	红包雨游戏，某企业有100名员工，员工的工号依次是1, 2，3, 4，..到100。现在公司举办了年会活动，活动中有一个红包雨环节，要求共计发出200个红包雨。其中小红包在[1 - 30]
	元之间，总占比为80%，大红包[31-100]元，总占比为20%。
		具体的功能点如下：
			系统模拟上述要求产生200个红包
			模拟100个员工抢红包雨，需要输出哪个员工抢到哪个红包的过程，活动结束时需要提示活动结束
			活动结束后，请1对100名员工按照所抢红包的总金额进行降序排序展示
*/
	
	public static void main(String[] args) throws Exception {
		List<Double> redMoney = generateMoney(200);
		Collections.shuffle(redMoney);
		List<PeopleThread> people = new ArrayList<>();
		for (int i = 1; i <= 100; i++) {
			
			PeopleThread pt = new PeopleThread(redMoney, "员工" + i);
			people.add(pt);
			pt.start();
		}
		for (PeopleThread person : people) {
			person.join();
		}
		System.out.println("---------------------------");
		people.stream().sorted((p1, p2) -> Double.compare(p2.getTotalMoney(),
			p1.getTotalMoney())).forEach(i -> System.out.println(i.getName() + ":\t" + i.getTotalMoney()));
	}
	
	public static List<Double> generateMoney(int sum) {
		Random r = new Random();
		DecimalFormat df = new DecimalFormat("#.00");
		List<Double> redEnv = new ArrayList<>();
		for (int i = 1; i <= sum * 0.8; i++) {
			redEnv.add(Double.parseDouble(df.format(r.nextDouble(30) + 1)));
		}
		for (int i = 1; i <= sum * 0.8; i++) {
			redEnv.add(Math.floor(Double.parseDouble(df.format(r.nextDouble(70) + 31))));
		}
		return redEnv;
	}
}
