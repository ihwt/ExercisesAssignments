package com.Projects.Exam.demo44;

import java.util.List;
import java.util.Random;

public class PeopleThread extends Thread {
	private final List<Double> redMoney;
	private final Random r = new Random();
	private double totalMoney;
	
	public PeopleThread(List<Double> redMoney, String name) {
		super(name);
		this.redMoney = redMoney;
	}
	
	@Override
	public void run() {
		while (true) {
			synchronized (redMoney) {
				if (redMoney.isEmpty()) {
					break;
				}
				double money = redMoney.remove(r.nextInt(redMoney.size()));
				System.out.println(super.getName() + "抢到了" + money);
				totalMoney += money;
				try {
					Thread.sleep(100);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	public double getTotalMoney() {
		return totalMoney;
	}
	
	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}
}
