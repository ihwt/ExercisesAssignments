package com.Projects.CopyFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Copy {
	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new FileReader("Java_advanced\\src\\com\\ihu\\IOflow\\1.txt"));
		     BufferedWriter bw = new BufferedWriter(new FileWriter("Java_advanced\\src\\com\\ihu\\IOflow\\2.txt",
			     true));) {
			ArrayList<String> list = new ArrayList<>();
			
			String data;
			while ((data = br.readLine()) != null) {
				list.add(data);
			}
			Collections.sort(list);
			for (String s : list) {
				bw.write(s);
				bw.newLine();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
