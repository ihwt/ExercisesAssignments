package com.Projects.Threading;

import java.util.List;
import java.util.Random;

public class SendTest extends Thread {
	private final List<String> list;
	Random r = new Random();
	private int count;
	
	public SendTest(List<String> list, String name) {
		super(name);
		this.list = list;
	}
	
	@Override
	public void run() {
		while (true) {
			synchronized (list) {
				if (list.size() <= 10) {
					break;
				}
				String l = list.remove(r.nextInt(list.size()));
				System.out.println(Thread.currentThread().getName() + " " + l);
				count++;
			}
		}
	}
	
	public List<String> getList() {
		return list;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
}
