package com.Projects.Threading;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Test {
	public static void main(String[] args) throws Exception {
		Random r = new Random();
		List<String> list = new ArrayList<>();
		
		String[] lift = {"口红", "包包", "鲜花", "剃须刀", "皮带", "手表"};
		
		for (int i = 1; i <= 100; i++) {
			list.add(lift[r.nextInt(lift.length)] + i);
		}
		
		SendTest xm = new SendTest(list, "小明");
		xm.start();
		SendTest xh = new SendTest(list, "小红");
		xh.start();
		
		xm.join();
		xh.join();
		
		System.out.println(xm.getCount() + "---" + xh.getCount());
		
		
	}
}
